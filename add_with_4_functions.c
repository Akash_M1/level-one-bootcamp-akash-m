//Write a program to add two user input numbers using 4 functions.

#include <stdio.h>
float input()
{
    float a;
    printf("enter a number\n");
    scanf("%f",&a);
    return a;
}
float find_sum(float a,float b)
{
    float sum;
    sum=a+b;
    return sum;
}
float output(float a,float b,float sum)
{
    printf("Sum of %f + %f is %f",a,b,sum);
    return 0;
}
int main()
{
    float x=input();
    float z=input();
    float y=find_sum(x,z);
    output(x,z,y);
    return 0;
}
