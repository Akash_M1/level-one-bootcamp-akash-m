//WAP to find the distance between two point using 4 functions.

#include <stdio.h>
#include <math.h>
float input()
{
    int a;
    float x;
    for(a=1;a<=4;a++)
    if(a==1)
    {
        printf("Enter the value of abcissa of first point\n");
        scanf("%f",&x);
    }
    else if(a==2)
    {
        printf("Enter the value of ordinate of  first point\n");
        scanf("%f",&x);  
    }
    else if(a==3)
    {
        printf("Enter the value of abcissa of second point\n");
        scanf("%f",&x); 
    }
    else if(a==4)
    {
        printf("Enter the value of ordinate of  second point\n");
        scanf("%f",&x);  
    }
    else
    {
        printf("Invalid entry");
    }
    return x;
}
float Find_distance(float x1,float y1,float x2,float y2)
{
    float distance;
    distance=sqrt(pow((x1-y1),2)+pow((x2-y2),2));
    return distance;
}
void output(float distance)
{
    printf("Distance between two points is %f ",distance);
}
int main()
{
    float x1,y1,x2,y2,distance;
    x1=input();
    y1=input();
    x2=input();
    y2=input();
    distance=Find_distance(x1,y1,x2,y2);
    output(distance);
    return 0;
}