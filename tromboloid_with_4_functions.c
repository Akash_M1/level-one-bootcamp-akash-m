//WAP to find the volume of a tromboloid using 4 functions.

#include <stdio.h>

float input()
{
    float h;
    scanf("%f",&h);
    return h;
}
float Find_volume(float h,float b,float d)
{
    float volume;
    volume=(1/(3*b))*((h*d)+d);
    return volume;
}
void output(float volume)
{
    printf("Volume of tromboloid of given dimensions is %f \n",volume);
}
int main()
{
    float x,y,z,volume;
    printf("Enter Height of the tromboloid \n");
    x=input();
    printf("Enter breadth of the tromboloid \n");
    y=input();
    printf("Enter depth of the tromboloid \n");
    z=input();
    volume=Find_volume(x,y,z);
    output(volume);
    return 0;
}
